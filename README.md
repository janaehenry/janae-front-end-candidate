# IS Front-End Candidate

## Instructions

Thanks for your interest in becoming a front-end developer at Interactive Strategies. You are here because we think you're pretty great, and we appreciate your time and willingness to participate in this exercise.

Using the [design in Figma](https://www.figma.com/file/WobeHiVIHlhsIGrTOBbTtO/IS-CODE-COMPONENTS), build the two components using semantic markup with a mobile-first approach for responsiveness using JavaScript/jQuery where needed. The CSS/SCSS _**must**_ be hand-coded without leveraging CSS frameworks, such as Tailwind or Bootstrap. Please keep performance and accessibility in mind.

The accordion items should all be closed on initial page load, and any number of accordion items can be open at any given time.

There is a style guide to reference for colors, typography and link styles.

All image and font assets have already been exported for your use. We use [Icomoon](icomoon.io) for our icons, and the `@font-face` declaration and minimal amount of CSS required for them to appear is already present in `style.scss`.

After you have finished, please commit to the `main` branch of the repository, and then email Dave (dave@interactivestrategies.com) and Jameel (jameel@interactivestrategies.com).

If you have any issues running this project locally, please don't hesitate to reach out to Dave or Jameel.

## Directory Structure

```
dist/ __________________ # Compiled Code & Files
gulpfile.esm.js/ _______ # Gulp
src/ ___________________ # Pre-Compiled Source Code & Files
|- images/ _____________ # Images
|- index.html___________ # HTML File
|- scripts.js __________ # JavaScript File
|- style.scss __________ # SCSS File
```

## Prerequisites

| Name   | Version    | Documentation       |
|--------|------------|---------------------|
| Node   | 14 or 15   | http://nodejs.org   |

## Installing Dependencies

To install the required dependencies to build the project locally:

#### With [Yarn](https://classic.yarnpkg.com/lang/en):

```
$ yarn
```

#### With [NPM](https://docs.npmjs.com):

```
$ npm i
```

## Local Development

To run Gulp to build the project locally:

#### With Yarn:

```
$ yarn serve
```

#### With NPM:

```
$ npm run serve
```

After starting Gulp, you can navigate to http://localhost:3000 while developing to have live reload for HTML/JS changes with CSS changes streamed into the browser on the fly.

## Conventions

- Use relative links in CSS instead of root relative links for paths. For example, use `../images/image.jpg` instead of `/images/image.jpg`
- Use 2 spaces for indentation in all files. Don't use hard tabs
- Insert blank lines between blocks of code in SCSS and JS and between major blocks in HTML

## Notes

#### Author

Interactive Strategies - <frontend@interactivestrategies.com>

#### License

This project is licensed under the MIT License
