/* eslint-env node */
const $ = require('gulp-load-plugins')();
import beep from 'beepbeep';
import log from 'fancy-log';
import colors from 'ansi-colors';

// -----------------------------------------------------------------------------
//   Creates a Config Object to be Used Throughout Gulpfile and Tasks
// -----------------------------------------------------------------------------
export default {
  src: process.cwd() + '/src',
  dist: process.cwd() + '/dist',
  reportError(error) {
    const lineNumber = (error.lineNumber) ? `LINE ${error.lineNumber} -- ` : '';

    $.notify({
      title: 'Task Failed',
      message: `${lineNumber}Task Failed[${error.plugin}]`,
      sound: 'Sosumi'
    }).write(error);

    beep();

    let report = '';
    const chalk = (str) => colors.bgRed(str);

    report += chalk('TASK:') + ' [' + error.plugin + ']\n';
    report += chalk('PROB:') + ' ' + error.message + '\n';
    if (error.lineNumber) {
      report += chalk('LINE:') + ' ' + error.lineNumber + '\n';
    }
    if (error.fileName) {
      report += chalk('FILE:') + ' ' + error.fileName + '\n';
    }
    log.error(report);

    this.emit('end');
  }
};
