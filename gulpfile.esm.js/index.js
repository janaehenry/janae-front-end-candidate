import { task, src, dest, series, parallel, watch } from 'gulp';
const $ = require('gulp-load-plugins')();
import config from './gulp-config';
import log from 'fancy-log';
import colors from 'ansi-colors';

// For clean
import del from 'del';

// For SCSS
const sass = require('gulp-sass')(require('sass'));
import Fiber from 'fibers';
import autoprefixer from 'autoprefixer';

// Create a BrowserSync Instance
const browserSync = require('browser-sync').create('Local Server');

// Re-usable function to copy files
const copyFiles = (srcGlob, destPath) => {
  return src(srcGlob)
    .pipe($.plumber({
      errorHandler: config.reportError
    }))
    .pipe(dest(destPath))
    .on('end', browserSync.reload)
  ;
};


// -----------------------------------------------------------------------------
//
//  Task: Clean
//
//  Deletes the dist folder containing compiled output
//
// -----------------------------------------------------------------------------
task('clean', () => del([config.dist]));


// -----------------------------------------------------------------------------
//
//  Task: Styles
//
//  Compiles SCSS into CSS
//
// -----------------------------------------------------------------------------
task('styles', () => src(`${config.src}/style.scss`)
  .pipe($.plumber({
    errorHandler: config.reportError
  }))
  .pipe(sass({
    fiber: Fiber,
    outputStyle: 'expanded',
    errLogToConsole: true
  }))
  .on('error', config.reportError)
  .pipe($.postcss([
    autoprefixer({
      cascade: false,
      grid: true,
      overrideBrowserslist: [
        'Last 2 Chrome major versions',
        'Last 2 Edge major versions',
        'Last 2 Safari major versions',
        'Last 2 Firefox major versions',
        'Last 2 Samsung major versions',
        'Last 2 iOS major versions',
        'Last 2 Android major versions',
        'Last 2 ChromeAndroid major versions',
        'Last 2 FirefoxAndroid major versions',
        'not dead'
      ]
    })
  ]))
  .pipe(dest(`${config.dist}/assets/css/`))
  .pipe(browserSync.stream({match: '**/*.css'}))
);


// -----------------------------------------------------------------------------
//
//  Task: JS
//
//  Copies JS to dist and reloads
//
// -----------------------------------------------------------------------------
task('js', () => copyFiles(`${config.src}/scripts.js`, `${config.dist}/assets/js/`));


// -----------------------------------------------------------------------------
//
//  Task: Images
//
//  Copies images to dist
//
// -----------------------------------------------------------------------------
task('images', () => copyFiles(`${config.src}/images/**/*`, `${config.dist}/assets/images/`));


// -----------------------------------------------------------------------------
//
//  Task: Fonts
//
//  Copies fonts to dist
//
// -----------------------------------------------------------------------------
task('fonts', () => copyFiles(`${config.src}/fonts/**/*`, `${config.dist}/assets/fonts/`));


// -----------------------------------------------------------------------------
//
//  Task: HTML
//
//  Copies HTML to dist and reloads
//
// -----------------------------------------------------------------------------
task('html', () => copyFiles(`${config.src}/index.html`, config.dist));


// -----------------------------------------------------------------------------
//
//  Task: Watch
//
//  Starts BrowserSync and watches files for changes
//
// -----------------------------------------------------------------------------
task('watch', (done) => {

  // Start BrowserSync
  browserSync.init({
    open: false,
    logPrefix: 'BrowserSync',
    server: {
      baseDir: 'dist/'
    }
  });

  // Watch JS file for changes
  watch([`${config.src}/scripts.js`], series('js'));

  // Watch HTML file for changes
  watch([`${config.src}/index.html`], series('html'));

  // Watch SCSS file for changes
  watch(`${config.src}/style.scss`, series('styles'));

  // Watch images for changes
  watch(`${config.src}/images/**/*`, series('images'));

  // Watch fonts for changes
  watch(`${config.src}/fonts/**/*`, series('fonts'));

  done();
});


// -----------------------------------------------------------------------------
//
//  Task: Serve
//
//  Build and serve the project locally
//
// -----------------------------------------------------------------------------
task('serve', series('clean', parallel('styles', 'html', 'js', 'images', 'fonts'), 'watch'));


// -----------------------------------------------------------------------------
//   Don't run `gulp` directly
// -----------------------------------------------------------------------------
task('default', (done) => {
  log.error('\n\n[' + colors.red('ERROR') + '] This is not the proper way to run gulp. Please run ' + colors.green('"yarn serve"') + ' or ' + colors.green('"npm run serve"') + ' instead.\n');
  done();
  process.exit(1);
});
